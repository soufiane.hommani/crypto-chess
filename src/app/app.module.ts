import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SquareComponent } from './chessboard/square/square.component';
import { BoardComponent } from './chessboard/board/board.component';
import { KnightComponent } from './chessboard/pieces/knight/knight.component';
import { ChessboardContainerComponent } from './chessboard/chessboard-container/chessboard-container.component';

@NgModule({
  declarations: [
    AppComponent,
    SquareComponent,
    BoardComponent,
    KnightComponent,
    ChessboardContainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
